import unittest
from module import generate_password, check_password_level

class test_password(unittest.TestCase):

    def setUp(self):
        pass

    def test_len_298_complex_1(self):
        """Test a 298 length password with complexity 1"""
        password = generate_password(298, 1)
        self.assertEqual(check_password_level(password), 1)

    def test_len_22_complex_2(self):
        """Test a 22 length password with complexity 2"""
        password = generate_password(22, 2)
        self.assertEqual(check_password_level(password), 2)

    def test_len_10_complex_3(self):
        """Test a 10 length password with complexity 3"""
        password = generate_password(10, 3)
        self.assertEqual(check_password_level(password), 3)

    def test_len_526_complex_4(self):
        """Test a 526 length password with complexity 4"""
        password = generate_password(526, 4)
        self.assertEqual(check_password_level(password), 4)

    def test_len_2_complex_3(self):
        """Test a 2 length password with complexity 3"""
        password = generate_password(2, 3)
        self.assertEqual(check_password_level(password), -1)

    def test_len_1_4(self):
        """Test a 1 length password with complexity 4"""
        password = generate_password(1, 4)
        self.assertEqual(check_password_level(password), -1)

if __name__ == '__main__':
    unittest.main()
