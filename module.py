import random  # https://docs.python.org/3.6/library/random.html
import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
from sqlite3 import Error
import string  # https://docs.python.org/3.6/library/string.html
import requests

def generate_password(length: int, complexity: int) -> str:
    """Generate a random password with given length and complexity

    Complexity levels:
        Complexity == 1: return a password with only lowercase chars
        Complexity ==  2: Previous level plus at least 1 digit
        Complexity ==  3: Previous levels plus at least 1 uppercase char
        Complexity ==  4: Previous levels plus at least 1 punctuation char
    """

    if length < complexity:
        print("Must have length at least equal to complexity")
        return ""

    def shuffleString(inputStr):
        """Method to shuffle a string"""
        strList = list(inputStr)
        random.shuffle(strList)
        shuffledStr = "".join(strList)
        return shuffledStr

    lowercase = string.ascii_lowercase
    uppercase = string.ascii_uppercase
    digits = string.digits
    punctuation = string.punctuation

    password = ""

    if complexity == 1:
        password = "".join(random.SystemRandom().choice(lowercase)
                           for _ in range(length))
    elif complexity == 2:
        password = "".join(random.SystemRandom().choice(lowercase + digits)
                           for _ in range(length - 1))
        password += random.SystemRandom().choice(digits)
        password = shuffleString(password)
    elif complexity == 3:
        password = "".join(random.SystemRandom().choice(
            lowercase + digits + uppercase) for _ in range(length - 2))
        password += random.SystemRandom().choice(digits)
        password += random.SystemRandom().choice(uppercase)
        password = shuffleString(password)
    elif complexity == 4:
        password = "".join(random.SystemRandom().choice(
            lowercase + digits + uppercase + punctuation)
                           for _ in range(length - 3))
        password += random.SystemRandom().choice(digits)
        password += random.SystemRandom().choice(uppercase)
        password += random.SystemRandom().choice(punctuation)
        password = shuffleString(password)
    else:
        print("Invalid complexity level")
        return ""

    return password


def check_password_level(password: str) -> int:
    """Return the password complexity level for a given password

    Complexity levels:
        Return complexity 1: If password has only lowercase chars
        Return complexity 2: Previous level condition and at least 1 digit
        Return complexity 3: Previous levels condition and at least 1 uppercase char 
        Return complexity 4: Previous levels condition and at least 1 punctuation

    Complexity level exceptions (override previous results):
        Return complexity 2: password has length >= 8 chars and only lowercase chars
        Return complexity 3: password has length >= 8 chars and only lowercase and digits
    """

    if len(password) == 0:
        return -1

    level = 1

    for i in range(len(password)):
        if password[i].islower():
            continue
        elif password[i].isdigit():
            if level < 2:
                level = 2
        elif password[i].isupper():
            if level < 3:
                level = 3
        else:
            level = 4
            break
    return level


def create_user(db_path: str) -> None:
    """Retrieve a random user from https://randomuser.me/api/
    and persist the user (full name and email) into the given SQLite db
    """
    user = requests.get('https://randomuser.me/api/')
    user = user.json()
    first_name = user['results'][0]['name']['first']
    last_name = user['results'][0]['name']['last']
    email = user['results'][0]['email']

    data = (first_name + " " + last_name, email)

    try:
        conn = sqlite3.connect(db_path)
        cursor = conn.cursor()
        cursor.execute('insert into users(name, email) values (?, ?)', data)
        conn.commit()
    except Error as e:
        print(e)
    finally:
        conn.close()

create_user('storage.db')
