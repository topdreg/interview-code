import random
import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
from sqlite3 import Error
from module import generate_password, create_user

"""
for i in range(10):
    create_user('storage.db')
"""

query = """select * from users limit 10;"""

try:
    conn = sqlite3.connect('storage.db')
    cursor = conn.cursor()
    cursor.execute(query)
    info = cursor.fetchall()
    for user in info:
        pw_length = random.randint(6, 12)
        pw_complexity = random.randint(1, 4)
        password = generate_password(pw_length, pw_complexity)
        data = (user[0], password)
        cursor.execute('insert into passwords(person_id, password)
                       values (?, ?)', data)
        conn.commit()
except Error as e:
    print(e)
finally:
    conn.close()
