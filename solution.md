My solution is divided into three files: module.py, test_password.py, and get_users.py.

module.py contains the methods as desired from steps 2, 3, and 5.
test_password.py deals with step 4.
get_users.py deals with step 6.

My solution only uses the standard library in Python.

generate_password:
I feel like the solution for generate_password came about naturally. The module "random" really does the work for me. I use systemRandom() of random because it's supposedly better for cryptographic purposes. With each complexity level, I would append the required character at the end of the string and then shuffle it. I put the shuffling in its own method, since that kept being repeated. Beyond that though, I don't believe much would have been saved by modularizing anything else within the method.

Python, unfortunately, does not seem to have a case-switch syntax, which would have looked nice. Oh, and of note is that if length is less than complexity, then I return an empty string with a printed statement saying that such inputs are not okay, which would help with debugging. A more custom error message may be appropriate.


check_password_level:
I think check_password_level is self-explanatory. While looping through the password it uses string methods to check each character and adjust the return value accordingly. Of note is the -1 return value to tell the user if no password was passed in, which I hope is sufficient. I don't think there was much to do on this problem.


create_user:
I think this is self-explanatory also. I connect to the database and then insert values from the API. Connecting to the database could be its own method, in its own python module, separated from this method, but for the purposes of this programming challenge I thought that would have made things a bit confusing. So I opted to put the database connection code in the method itself. I've put in some standard error checking with connecting to the database, to show the user what may have went wrong.

test_password.py:
I use the standard unittest framework here for testing the generate_password method. It's just a bunch of assertEqual statements with check_password_level. I think I got a good range - large and small lengths, and varying complexities, including complexities that are larger than the length of the password.

get_users.py:
This is self-explanatory like create_user. Same logic with connecting and closing the database. I have a commented out line of code to use with inputting 10 users into the database (I had to put that somewhere - generally, I would put that in a separate module, but I didn't want to bloat the directory).  The query is simple - it just gets the first 10 users. The prompt did not state whether the users to be retrieved should be randomly selected or something specific, so I just choose the first 10 users.


